﻿using UnityEngine;
using System.Collections;

public class ShowAndHide : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Hide ();
	}

	void Hide(){
		foreach (GameObject gm in GameObject.FindGameObjectsWithTag("Pickups"))
		{
			
			gm.GetComponent<Renderer>().enabled = false;
			Destroy(gm);
		}
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.C)) {
			ShowOrHide(KeyCode.C);
		}
	}

	void ShowOrHide(KeyCode code){

		if (code == KeyCode.C) {
			foreach (GameObject gm in GameObject.FindGameObjectsWithTag("Pickups"))
			{
				
				gm.GetComponent<Renderer>().enabled = !gm.GetComponent<Renderer>().enabled;
			}
		}

	}
}
