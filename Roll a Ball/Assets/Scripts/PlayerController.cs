﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
	// player attribute vars
	private Rigidbody rb;
	public new static string name = null;
    public float speed;
    public float thrust;
    public float maxSpeed;
	private float speedOffset;
	private float growScaler;
	private float speedFactor = 1 / 1600f; // increase to decrease speed
	private float playerCollisionSizeScaler = 0.25f; // percentage of loser that winner keeps!
	
	// camera vars
    public new GameObject camera;
    public float zoomSpeed = 5;
    public float minFOV = 20;
    public float maxFOV = 100;
    private float currentFOV;
    private float targetFOV = 0;
    private Vector3 currentPosition;
    public Vector3 targetPosition;
    private Vector3 screenPoint;

	// bullet vars
	private bool shoot = true;
	private float bullet_time = 0.0f;
	private float bulletCooldownTime = 0.2F; // in seconds
	public bool dead = false;
	public float bullet_collision_enabled_time;
	public Object projectile;
	public float bulletPowerPercent = 0.05f;
	public static float bulletSpeed = 50.0f;

	// miscellaneous vars
	private Vector3 textSize;
	public GameObject text;
	private Transform[] children;
	private float time = 0.0f;
	private bool collidable = false;
	
	// Init function.
    void Start()
    {
		Scoring.score = 0;
        rb = GetComponent<Rigidbody>();
        currentFOV = 78f;
		currentPosition = this.camera.transform.position;

		// store the player's child objects for later reinstantiation
		children = new Transform[transform.childCount];
		int i = 0;
        foreach (Transform t in transform)
        {
            children[i++] = t;
        }

        textSize = this.transform.localScale;

        // set initial movement vars
        thrust = 100;
        maxSpeed = 20;
        growScaler = 0.05f;
		speedOffset = 2.5f;

    }

	void FixedUpdate()
	{
		// Ensure this is our client.
		if (this.GetComponent<PhotonView> ().isMine) {
			// Process bullet timer
			if (!shoot) {
				if (bullet_time >= 2) {
					shoot = true;
					bullet_time = 0.0f;
				} else {
					bullet_time += Time.deltaTime;
				}
			}
			time += Time.deltaTime;
			if (time >= 0.5f) {
				collidable = true;
				rb.constraints = RigidbodyConstraints.FreezePositionY;
			} 

			// Movement mangement!
			Vector3 movement = Vector3.zero;
		
			// touch input
			if (Manager.touchInput && Input.touchCount > 0) {
				Vector3 touchPosition = Input.GetTouch (0).position - new Vector2 (0, Screen.height);
				Ray target = Camera.current.ScreenPointToRay (touchPosition);
				movement = new Vector3 (target.direction.x, 0, target.direction.z);
			}
			// accelerometer input
			else if (!Manager.touchInput) {
				movement = new Vector3 (Input.acceleration.x, 0.0f, Input.acceleration.y);
			}
			// keyboard input (prioritised)
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");
			if (moveVertical != 0 || moveHorizontal != 0) {
				movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
			}
			// add the force to the player!
			rb.AddForce (movement * thrust);
		
			// limit speed
			float thisSpeed = CurrentSpeed();
			if (rb.velocity.magnitude >= thisSpeed) {
				rb.velocity = rb.velocity.normalized * thisSpeed;
			}
			// check if player wants to shewt!
			if (Input.GetKeyDown (KeyCode.Semicolon) && shoot) {
				// notify other players of this new bullet
				GetComponent<PhotonView> ().RPC ("SpawnBullet", PhotonTargets.All, transform.position, rb.velocity.x, rb.velocity.z, PhotonNetwork.player.ID);
				shoot = false;
			}        
		}
	}

	// Calculates the current speed of the player 
	public float CurrentSpeed() {
		return 1 / Mathf.Sqrt (this.transform.localScale.x * speedFactor) - speedOffset;
	}

	// This function is run every time the player eats some food.
	public void GrowSize(Vector3 size)
	{
		this.transform.localScale += size; // increase size
		// adjust the camera position appropriately.
		if (size.y > 0) {
			targetPosition = new Vector3 (camera.gameObject.transform.position .x, 0, camera.transform.position.z) + new Vector3 (0, 10 + this.gameObject.GetComponent<SphereCollider> ().radius, 0);
		} else if(size.y < 0) {
			targetPosition = new Vector3 (camera.gameObject.transform.position .x, 0, camera.transform.position.z) + new Vector3 (0, this.gameObject.GetComponent<SphereCollider> ().radius - 10, 0);
		}

	}
	
	void Update()
		
	{ 		
		/////////////////////////////
		// RE-ENABLE FOR NAME DISPLAY
		/////////////////////////////
		//float y = this.transform.Find("TextObject").position.y;
		//float z = this.transform.Find("TextObject").position.z;
		this.transform.Find("TextObject").GetComponent<TextMesh>().text = name;
		this.transform.Find("TextObject").position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
		this.transform.Find("TextObject").rotation = Quaternion.identity * Quaternion.Euler(new Vector3(90, 0, 0));
		this.transform.Find("TextObject").localScale = textSize;
		/////////////////////////////
		
		// ensure smooth adjustment of FOV and camera height!
		if (currentFOV <= targetFOV)
		{
			Camera.main.fieldOfView = Mathf.Lerp(currentFOV, targetFOV, Time.deltaTime * 5);
			currentFOV = Camera.main.fieldOfView;
		}
		if (currentPosition.y <= targetPosition.y)
		{
			camera.gameObject.transform.position = targetPosition;
			currentPosition = camera.gameObject.transform.position;
		}
		
		// Update score.
		if(this.GetComponent<PhotonView>().isMine) {
			Scoring.score = CurrentScore(Scoring.score);
		}
	}

	// This function calculates the current score
	public float CurrentScore(float hiScore) {
		return Mathf.Max (hiScore, this.transform.localScale.magnitude);
	}

	// This function is called for each collision detected by Unity.
	void OnCollisionEnter(Collision collision) {
		if (collidable) {
			
			/* Collision detection done by both the clients which are involve in the collision */
			if (collidable && (collision.gameObject.name == "PlayerCell" || collision.gameObject.name == "PlayerCell(Clone)")) {
				int owner_id = -1;
				int winner_id = -1;
				Vector3 size = Vector3.zero;
				
				/* Called on the winning client */
				if (PhotonNetwork.player.ID == this.gameObject.GetComponent<PhotonView> ().owner.ID) {
					// ensure this is the winning client
					if (this.gameObject.GetComponent<Transform> ().localScale.magnitude > collision.gameObject.GetComponent<Transform> ().localScale.magnitude) {
						if (this.gameObject == null || collision.gameObject == null) {
							// something went wrong.
							return;
						}
						// determine client identities
						owner_id = collision.gameObject.GetComponent<PhotonView> ().owner.ID;
						winner_id = this.gameObject.GetComponent<PhotonView> ().owner.ID;
						// calculate winner's reward!
						size = (playerCollisionSizeScaler) * collision.gameObject.transform.localScale;
					}
					// finally, update other players with the winner, loser and size gained.
					if (owner_id != -1 && winner_id != -1) {
						this.GetComponent<PhotonView> ().RPC ("DestroyPlayer", PhotonTargets.All, owner_id, winner_id, size);
					}
				}
			}
			
			// Check for bullet collisions
			if (PhotonNetwork.player.ID == this.gameObject.GetComponent<PhotonView>().owner.ID) {
				if (collision.gameObject.name == "Bullet" || collision.gameObject.name == "Bullet(Clone)") {
					// determine affected client
					int victim_id = this.gameObject.GetComponent<PhotonView>().owner.ID;
					float size_c = 0;
					// determine shooter
					int owner_id = collision.gameObject.GetComponent<BulletController>().owner_id;
					foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
						if (obj.GetComponent<PhotonView> ().owner.ID == victim_id) {
							size_c = obj.transform.localScale.magnitude;
						}
					}
					// determine amount to grow the shooter
					float sizeChange = bulletPowerPercent * size_c;
					
					// update all players with this information!
					this.gameObject.GetComponent<PhotonView> ().RPC ("BulletNotice", PhotonTargets.AllBuffered, owner_id, victim_id, sizeChange);
				}
			}
		}
	}

	// Process food pickups.
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Pickups") || other.gameObject.CompareTag("StaticObject"))
		{
			other.gameObject.SetActive(false);
			this.GrowSize(new Vector3(growScaler, growScaler, growScaler)); // make dem bigger!!
			AddColorEffect(other.GetComponent<Renderer>().material.color);
		}
	}

	// Function for determining the player's colour.
	void AddColorEffect(Color otherColor)
	{
		Color lastColor = this.GetComponent<Renderer>().material.color;
		this.GetComponent<Renderer>().material.color = (lastColor + otherColor) / 2;
		this.GetComponent<Light>().color = (lastColor + otherColor) / 2;
		this.GetComponent<Light>().range = this.gameObject.transform.localScale.x + 10;
		this.GetComponent<TrailRenderer>().material.color = (lastColor + otherColor) / 2;
		
	}
	// A way out!
	void OnDisconnectedFromPhoton() {
		Application.LoadLevel ("ScoringScene");
	}

	// Network function for creating a new bullet in the world
	[PunRPC]
	IEnumerator SpawnBullet(Vector3 position, float x, float z, int owner_id) {
		GameObject bullet = (GameObject)Instantiate(projectile, position, Quaternion.identity);
		bullet.GetComponent<BulletController> ().owner_id = owner_id;
		bullet.GetComponent<BulletController>().target = new Vector3(x*1000, 3.2f, z*1000);
		yield return new WaitForSeconds (bulletCooldownTime);
		bullet.GetComponent<SphereCollider> ().enabled = true;
	}

	[PunRPC]
	void DestroyPlayer(int owner_id, int winner_id, Vector3 size) {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
			if (obj.GetComponent<PhotonView> ().owner.ID == owner_id) {
				Destroy (obj);
			}
			if(obj.GetComponent<PhotonView>().owner.ID == winner_id) {
				obj.GetComponent<PlayerController>().GrowSize(size);
			}
		}
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Camera")) {
			if (obj.GetComponent<PhotonView> ().owner.ID == owner_id) {
				Destroy (obj);
			}
		}
		if (PhotonNetwork.player.ID == owner_id) {
			Debug.Log("You are disconnecting");
			PhotonNetwork.Disconnect ();
		}
	}
	// TODO: determine necessity
	[PunRPC]
	void BulletDeath(int shooterID) {
		// kill the bullet
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Bullet")) {
			if (obj.GetComponent<PhotonView> ().owner.ID == shooterID) {
				PhotonNetwork.Destroy (obj);
			}
			Destroy (obj);
		}
	}
	// The network function for updating clients of a bullet collision.
	[PunRPC]
	void BulletNotice (int shooterID, int victimID, float sizeChange) {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
			if (obj.GetComponent<PhotonView> ().owner.ID == shooterID) {
				obj.transform.localScale += new Vector3 (sizeChange, sizeChange, sizeChange);
			}
			if (obj.GetComponent<PhotonView> ().owner.ID == victimID) {
				obj.transform.localScale -= new Vector3 (sizeChange, sizeChange, sizeChange);
			}
		}
	}

	// UNUSED FUNCTION AS YET? TODO: DETERMINE NECESSITY.
	[PunRPC]
	void NotifyDeath(int owner_id) {
		if (PhotonNetwork.player.ID == owner_id) {
			PhotonNetwork.Disconnect ();
		}
	}
	
	// This function ensure's each player is viewing themselves over the network.
	[PunRPC]
	void SetCamera(int ownerID) {

		// Camera and player are the new things that just got instantiated
		GameObject camera = null;
		GameObject player = null;
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
			if(obj.gameObject.GetComponent<PhotonView>().owner.ID == ownerID) {
				player = obj;
			}
		}
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Camera")) {
			if (obj.gameObject.GetComponent<PhotonView> ().owner.ID == ownerID) {
				camera = obj;
			}
		}
		// This must be our camera.
		if (PhotonNetwork.player.ID == ownerID) {
			camera.transform.Rotate (new Vector3 (90, 0, 0));
			camera.SetActive (true);
			player.SetActive (true);
			camera.GetComponent<CameraController> ().player = player;
			player.GetComponent<PlayerController> ().enabled = true;
			player.GetComponent<PlayerController> ().camera = camera;
		} else {
			camera.GetComponent<CameraController> ().player = player;
			player.GetComponent<PlayerController> ().camera = camera;
			camera.SetActive(false);
		}
	}
}