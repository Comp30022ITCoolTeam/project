﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour {
	
	public CanvasGroup exitModal;
	public CanvasGroup gameOver;
	
	public void StartGame()
	{
		Application.LoadLevel("MapScene");
	}
	
	public void OpenSettings()
	{
		Application.LoadLevel ("SettingsScene");
	}
	
	//This must be seperate to loading the ScoringScene level.
	//Otherwise PhotonNetwork.Disconnect will work in the background and would be
	//interrupted by loading the new level.
	public void confirmExit(){
		
		PhotonNetwork.Disconnect ();
	}
	
	//Now that the network is fully disconnected from, we can load the new level.
	void OnDisconnectedFromPhoton(){
		
		Application.LoadLevel ("ScoringScene");
	}
	
	public void OpenMenu()
	{
		
		Application.LoadLevel("MenuScene");
		
	}
	
	public void LoadLand() 
	{
		Application.LoadLevel ("Land");
	}
	
	public void LoadWater() 
	{
		Application.LoadLevel ("Water");
	}
	
	public void LoadCartoon() 
	{
		Application.LoadLevel ("Cartoon");
	}
	
	//The "modal" refers to the confirmation of exiting the game
	//This object exists throughout the whole game but is not visible or clickable
	//until this funciton is called
	public void ShowModal()
	{
		exitModal.interactable = true;
		exitModal.alpha = 1;
		exitModal.blocksRaycasts = true;
	}
	
	//Function to remove confirmation modal if player decides against exiting game.
	public void closeModal() {
		exitModal.interactable = false;
		exitModal.alpha = 0;
		exitModal.blocksRaycasts = false;
	}
	
	public void ShowGameOver()
	{
		
		gameOver.interactable = true;
		gameOver.alpha = 1;
		gameOver.blocksRaycasts = true;
	}
}