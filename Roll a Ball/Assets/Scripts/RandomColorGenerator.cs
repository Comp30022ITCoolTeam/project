﻿using UnityEngine;
using System.Collections;
using System;


public class RandomColorGenerator : MonoBehaviour {


	//Note this is just logic of color changing , but not perfect yet, but at least basic layout


	System.Random randomGen = new System.Random();
	Color[] colors = new Color[6];
	public int colorTime;

	Color currentColor;
	float intialTime = 0;

	void Start(){
		//Color.Lerp(prevColor, GetRandomColor (),0.1f);

		colors[0] = Color.cyan;
		colors[1] = Color.red;
		colors[2] = Color.green;
		colors[3] = new Color(255, 165, 0);
		colors[4] = Color.yellow;
		colors[5] = Color.magenta;


		this.currentColor = GetRandomColor ();
	}
	// Update is called once per frame
	void Update () {

		//this.GetComponent<Renderer> ().material.color = GetRandomColor ();

		intialTime += Time.deltaTime;
		GetComponent<Renderer> ().material.color = Color.Lerp(GetComponent<Renderer> ().material.color,
		                                                      currentColor, Time.deltaTime*5);
//		if (intialTime >= colorTime) {
//			intialTime = 0;
//			GetComponent<Renderer> ().material.color = Color.Lerp(GetComponent<Renderer> ().material.color,
//			                                                      GetRandomColor (), 0.9f);
//		}

		if (intialTime >= 5) {
			intialTime = 0;
			this.currentColor = GetRandomColor();
		}



	}


	#region Does not work preoperly
	public Color GetRandomColor(){
		int randomColorIndex = randomGen.Next(0,6);
		return this.colors [randomColorIndex];
	}
	
	#endregion

		                        
}
