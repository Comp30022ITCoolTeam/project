﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
	public Vector3 target;
	public int owner_id;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate((target - transform.position).normalized*PlayerController.bulletSpeed*Time.deltaTime);
	}
}