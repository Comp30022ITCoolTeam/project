﻿using UnityEngine;
using System.Collections;
using Helper;


public class StaticObjectGenerator : MonoBehaviour {
	
	#region Private varibles
	
	//colors for the spheres
	private Color[] colors;
	//time to generate object
	private float objectCreationtime;
	//time to change the color
	private float colorChangingTime;
	//max colors avaible 
	private int maxColor;
	//max items to generate on the map
	private int maxItemsOnMap;
	private int numberOfObjectsToGenetateOneTime;
	private bool done;
	private string tagToChangeColor;
	private int objectCreationRange;
	//y position for object which will be 
	//generated randomly on the map
	private float yPosition = 0.5f;
	#endregion
	
	#region Public varibles
	//they're being assigned in the unity interface
	public int colorFrequeny;
	public Transform staticObjectModel;
	public int objectFrequeny;
	
	#endregion
	


	//at the start of the game this 
	//methods initialize the private varibles
	void Start(){
		//initialize the varibles
		maxItemsOnMap = 5000;
		objectCreationRange = 500;
		tagToChangeColor = "StaticObject";
		numberOfObjectsToGenetateOneTime = 1000;
		done = false;
		objectCreationtime = 0;
		colorChangingTime = 0;
		//getting the colors
		colors = HelperMethods.GetColors ();
		//numbers of colors that can be used to 
		//change the color of spheres 
		maxColor = colors.Length;
 	}
	//update the map stage 
	//after some time only change color
	//will be called
	void Update(){
		GameObject [] items = GameObject.FindGameObjectsWithTag (tagToChangeColor);
		if (items.Length > maxItemsOnMap) {
			done = true;
		}
		if (!done) {
			GenerateObject (numberOfObjectsToGenetateOneTime);
		} 
		ChangeColor();
	}
	//This methods takes  number of sphere to generate
	//and assign the position randomly in the range mentioned
	void GenerateObject(int n){
		objectCreationtime += Time.deltaTime;
		if (objectCreationtime > objectFrequeny) {
			objectCreationtime = 0;
			for (int i = 0; i <n; i ++) {
				//here one sphere being generated in the range 
				Instantiate(staticObjectModel, new Vector3(Random.Range(-objectCreationRange,objectCreationRange),yPosition,
				Random.Range(-objectCreationRange,objectCreationRange)), Quaternion.identity);
            }
		}
	}
	//change the color of all the items 
	//with the tag mentioend in the start
	//method, It gets random color for every
	//existing item and assign it's material color
	void ChangeColor(){
		colorChangingTime += Time.deltaTime;
		if (colorChangingTime > colorFrequeny) {
			colorChangingTime = 0;
			foreach (GameObject gm in GameObject.FindGameObjectsWithTag(tagToChangeColor)) {
				Color color = GetRandomColor ();
				gm.GetComponent<Renderer> ().material.color = color;
				//Debug.Log(color.ToString());
			}
		}
	}

	//get the random colors from the colors array
	 Color GetRandomColor()
     {
		int randomColorIndex =  Random.Range(0,maxColor);
		return this.colors [randomColorIndex];
	 }
       
}


