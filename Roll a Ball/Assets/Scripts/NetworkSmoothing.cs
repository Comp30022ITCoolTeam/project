﻿/* Libraries required */
using UnityEngine;
using System.Collections;

/* Responsible for smoothing network movements */
public class NetworkSmoothing : Photon.MonoBehaviour {
	/* The following variables indicate the realPosition, realRotation and realScale of the object */
	/* This gets updated SEND_RATE times per second (Defined in NetworkManager.cs) */
	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;
	/* The percentage/ratio used for linear interpolation */
	public float RATIO = 0.2f;
	
	// Use this for initialization
	void Start () {
		// Nothing to initialize
	}
	
	// Update is called once per frame
	void Update () {
		/* Check if you recieved an update from yourself */
		if (photonView.isMine) {
			// Do nothing as your own position is correct
		} else {
			/* Only start interpolating 1 second after joining to avoid jerking from origin to final location */
			/* Since this is sent over the network, we interpolate from the player's current position to their most recent known position 
			 * to ensure we get smooth movement of players.
			 */
			if(Time.timeSinceLevelLoad >= 1) {
				transform.position = Vector3.Lerp(transform.position, realPosition, RATIO);
				transform.rotation = Quaternion.Lerp (transform.rotation, realRotation, RATIO);
			}
			else {
				transform.position = realPosition;
				transform.rotation = realRotation;
			}
		}
	}
	
	/* The following function is responsible for sending information about yourself and recieving information
	 * about other clients. 
	 */
	void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info) {
		/* We will send our color, light color, trail color, position, rotation and scale */
		Color color = GetComponent<Renderer> ().material.color;
		Color light_color = GetComponent<Light> ().color;
		Color trail_color = GetComponent<TrailRenderer> ().material.color;
		/* Check if we are sending */
		if (stream.isWriting) {
			/* Send our position */
			stream.SendNext(transform.position);
			/* Send our rotation */
			stream.SendNext (transform.rotation);
			/* Send our scale */
			stream.SendNext(transform.localScale);
			/* Send the color of our sphere */
			stream.SendNext (color.r);
			stream.SendNext(color.g);
			stream.SendNext(color.b);
			stream.SendNext(color.a);
			/* Send the color of our light */
			stream.SendNext (light_color.r);
			stream.SendNext(light_color.g);
			stream.SendNext(light_color.b);
			stream.SendNext(light_color.a);
			/* Send the color of our trail */
			stream.SendNext (trail_color.r);
			stream.SendNext(trail_color.g);
			stream.SendNext(trail_color.b);
			stream.SendNext(trail_color.a);
		}
		/* Check if we are receiving */
		if (stream.isReading) {
			/* Store the other player's position */
			realPosition = (Vector3)stream.ReceiveNext();
			/* Store the other player's rotation */
			realRotation = (Quaternion)stream.ReceiveNext();
			/* Store the other player's scale */
			transform.localScale = (Vector3)stream.ReceiveNext();
			/* Store the other person's color */
			float r = (float)stream.ReceiveNext();
			float g = (float)stream.ReceiveNext();
			float b = (float)stream.ReceiveNext();
			float a = (float)stream.ReceiveNext();
			/* Store the other person's light color */
			float light_r = (float)stream.ReceiveNext();
			float light_g = (float)stream.ReceiveNext();
			float light_b = (float)stream.ReceiveNext();
			float light_a = (float)stream.ReceiveNext();
			/* Store the other person's trail color */
			float trail_r = (float)stream.ReceiveNext();
			float trail_g = (float)stream.ReceiveNext();
			float trail_b = (float)stream.ReceiveNext();
			float trail_a = (float)stream.ReceiveNext();
			
			/* Apply the colors to the other player */
			GetComponent<Renderer>().material.color = new Vector4(r,g,b,a);
			GetComponent<Light>().color = new Vector4(light_r, light_g, light_b, light_a);
			GetComponent<TrailRenderer>().material.color = new Vector4(trail_r, trail_g, trail_b, trail_a);
		}
	}
}
