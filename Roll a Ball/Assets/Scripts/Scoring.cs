﻿/* Libraries required */
using UnityEngine;
using System.Collections;

/* This class just remembers the score for the client */
public class Scoring : MonoBehaviour {
	/* The score of this client */
	static public float score = 0.0f;
}
