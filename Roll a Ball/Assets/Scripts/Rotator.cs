﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {
	
	#region Private varibles
	private static Vector3 rotation =  new Vector3 (15, 30, 45);
	#endregion

	void Update () {
		//to rotate the spheres
		transform.Rotate (rotation*Time.deltaTime);
	}
}
