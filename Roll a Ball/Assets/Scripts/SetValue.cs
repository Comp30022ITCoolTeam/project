﻿using UnityEngine;
using System.Collections;

public class SetValue : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SetInitialSize ("Enemy", new Vector3 (2, 2, 2));
	}
	
	// Update is called once per frame
	void SetInitialSize(string tag, Vector3 size){
		foreach (GameObject gm in GameObject.FindGameObjectsWithTag(tag)) {
			gm.transform.localScale = size;
			Vector3 position = gm.transform.position;
			position.y = size.y/2;
			gm.transform.position = position;
		}
	}
}
