﻿using UnityEngine;
using System.Collections;

public class GroundScript : MonoBehaviour {

	// Use this for initialization
	private static Vector3 MAP_SIZE = new Vector3 (200,1,200);
	
	//on the start of the game this scrript is attached to the map
	//and it will assign the size of map
	void Start () {
		this.transform.localScale = MAP_SIZE;
	}
}
