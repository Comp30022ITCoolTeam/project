﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InputToggle : MonoBehaviour {

	//Both of the following functions simply ensure that if either toggle
	//is clicked, it either changes to on if it was off, or stays on if
	//it was on. Also that the buttons remain mutually exclusive at all times.
	
	//If Gyro toggle is clicked, ensure Touch isn't and Gyro is
	public void toggleGyro() {

		if (GameObject.Find ("Gyro").GetComponent<Toggle> ().isOn == true) {
			GameObject.Find ("Touch").GetComponent<Toggle> ().isOn = false;
			Manager.touchInput = false;
		} else if (GameObject.Find ("Touch").GetComponent<Toggle> ().isOn == false) {
			GameObject.Find ("Gyro").GetComponent<Toggle> ().isOn = true;
			Manager.touchInput = true;
		}
	}
	
	//If Touch toggle is clicked, ensure Gyro isn't and Touch is
	public void toggleTouch() {

		if (GameObject.Find ("Touch").GetComponent<Toggle> ().isOn == true) {
			GameObject.Find ("Gyro").GetComponent<Toggle> ().isOn = false;
			Manager.touchInput = true;
		} else if (GameObject.Find ("Gyro").GetComponent<Toggle> ().isOn == false) {
			GameObject.Find ("Touch").GetComponent<Toggle> ().isOn = true;
			Manager.touchInput = false;
		}
	}
}