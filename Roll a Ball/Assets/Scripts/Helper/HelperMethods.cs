﻿using UnityEngine;
using System.Collections;


namespace Helper{

	public class HelperMethods{

		public static Color[] GetColors(){
			Color[] colors = new Color[20];
            colors[0] = Color.cyan;
            colors[1] = Color.red;
            colors[2] = Color.green;
			colors [3] = Color.cyan;
            colors[4] = Color.gray;
            colors[5] = Color.magenta;
            colors[6] = Color.blue;


            //colors from Hexstring
            colors[7] = GetColor("#1c82c1");
            colors[9] = GetColor("#268bdo");
            colors[10] = GetColor("#cb28db");
            colors[11] = GetColor("#126e28");
            colors[12] = GetColor("#29137b");
            colors[13] = GetColor("#48e438");
            colors[14] = GetColor("#6f5b13");
            colors[15] = GetColor("#129677");
            colors[16] = GetColor("#622112");
            colors[17] = GetColor("#7bd41d");
            colors[18] = GetColor("#2e3209");
            colors[19] = GetColor("#e1232c");
          
			return colors;
		}
        public static Color GetColor(string hexString)
        {
            Color color;
            if(Color.TryParseHexString(hexString, out color)){
                return color;
            }
            return Color.cyan;
           
        }

	}

}
