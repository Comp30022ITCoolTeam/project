﻿/* Libraries required */
using UnityEngine;
using System.Collections;

/* Responsible for handling networking interactions */
public class NetworkManager : MonoBehaviour
{
	/* Constants, change if required */
	/* The amount of packets which will be sent per/second */
	public const int SEND_RATE = 50;
	/* The game version on the cloud */
	public string VERSION = "v0.2";
	/* The following string was used to allow testing on different servers/rooms */
	/* It is basically added to the level name when creating/joining a room */
	public string MODIFIER = "myversion";
	/* A boolean value which indicates whether the player will spawn random or at origin */
	public bool random_spawing_enabled = false;
	/* The initial X, Y, Z values if random spawining is disabled */
	public float x = 0;
	public float y =  1.996902f;
	public float z = 0;
	public float CAMERA_HEIGHT = 30.0f;
	/* Ranges for random spawning */
	public int MIN = -50;
	public int MAX = 50;
	
	/* Called when the game starts */
	void Start () {
		PhotonNetwork.sendRate = SEND_RATE;
		PhotonNetwork.sendRateOnSerialize = SEND_RATE;
		Debug.Log ("Start");
		Connect ();
	}
	
	/* Connects to the cloud server */
	void Connect () {
		Debug.Log ("Connect");
		PhotonNetwork.ConnectUsingSettings (VERSION);
	}
	
	/* Display connection information on the topleft of screen */
	void OnGUI () {
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
	}
	
	/* This function is called after ConnectUsingSettings is called and the master server is joined */
	void OnConnectedToMaster () {
		Debug.Log ("OnConnectedToMasterServer");
		PhotonNetwork.JoinLobby ();
	}
	
	/* This function is called after JoinLobby is called and a lobby is joined */
	void OnJoinedLobby () { 
		Debug.Log ("OnJoinedLobby");
		Debug.Log (Application.loadedLevelName);
		PhotonNetwork.JoinOrCreateRoom (Application.loadedLevelName + MODIFIER, null, null);
	}
	
	/* This is called after JoinOrCreateRoom is called and when a room is joined */
	void OnJoinedRoom () {
		Debug.Log ("OnJoinedRoom");
		SpawnMyPlayer ();
	}
	
	/* Spawns the player */
	void SpawnMyPlayer () {
		/* If random spawning is enabled spawn the player in the range */
		if (random_spawing_enabled) {
			x = Random.Range (MIN, MAX);
			z = Random.Range (MIN, MAX);
		}
		/* Instantiate the player on the network */
		GameObject player = PhotonNetwork.Instantiate ("PlayerCell", new Vector3 (x, y, z), Quaternion.identity, 0);
		Debug.Log ("Player instantiated");
		// Instantiate a camera for the player
		PhotonNetwork.Instantiate ("PlayerFollowCamera", new Vector3 (x, CAMERA_HEIGHT, z), Quaternion.identity, 0);
		Debug.Log ("Camera instantiated");
		player.GetComponent<PhotonView> ().RPC ("SetCamera", PhotonTargets.AllBuffered, player.GetComponent<PhotonView> ().owner.ID);
		Debug.Log ("Done");
	}
}
