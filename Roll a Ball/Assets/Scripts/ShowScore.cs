﻿/* Libraries required */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;

/* This class is responsible for showing and persisting the score */
public class ShowScore : MonoBehaviour {
	/* The filename to store the score in */
	public string FILE_NAME = "personal_best.txt";
	int score = 0;
	
	// Use this for initialization
	void Start () {
		/* Find the current score of the player */
		GameObject.Find ("CurrentScore").GetComponent<Text> ().text = ((int)Scoring.score).ToString ();
		/* Check if the file for the score already exists */
		if (File.Exists (FILE_NAME)) {
			/* The file exists so, read the score from the file */
			StreamReader inp_stm = new StreamReader (FILE_NAME);
			while (!inp_stm.EndOfStream) {
				string inp_ln = inp_stm.ReadLine ();
				score = int.Parse (inp_ln);
			}
			inp_stm.Close ();
			
			/* Check if score is higher than personal best */
			if(((int)Scoring.score) > score) {
				/* The score is higher, so rewrite the score in the file */
				var sr = File.CreateText(FILE_NAME);
				sr.WriteLine (((int)(Scoring.score)).ToString ());
				sr.Close();
				/* Update your personal best so it can be displayed on the score screen */
				GameObject.Find ("PersonalBest").GetComponent<Text>().text = ((int)Scoring.score).ToString();
			}
			/* The current score was less than your personal best so personal best will not be changed on the screen */
			else {
				GameObject.Find ("PersonalBest").GetComponent<Text>().text = ((int)score).ToString();
			}
		} 
		else {
			/* Write the high score to the file */
			var sr = File.CreateText(FILE_NAME);
			sr.WriteLine (((int)(Scoring.score)).ToString ());
			sr.Close();
			GameObject.Find ("PersonalBest").GetComponent<Text>().text = ((int)score).ToString();
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
