﻿/* Libraries required */
using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

/* The following class contains all the unit tests for our game */
/* Things tested for using unit tests:
 * 1. Player growth/shrinking on collisions
 * 2. Player speed increasing/decreasing on collisions
 * 3. Camera zooming/zooming out on collisions
 * 4. Player score increasing appropriately
 */
[TestFixture]
public class UnitTestCases  {
	/* 1. Ensuring the player grows/shrinks appropriately on collison.
	 * For this unit test, we will test the function GrowSize(amt) as that
	 * gets called on each collision with another player/bullet. We will
	 * check whether the player grows size when amt is positive, player decreases
	 * size when amt is negative and the player maintains their size when amt is zero.
	 */
	// Testing whether the player grows size when the amt is positive.
	[Test]
    public void SizeGrowsOnPositiveAmount() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial size
		float initial_size = player.transform.localScale.magnitude;
		// Increase the player's size by positive amt
		player.GetComponent<PlayerController> ().GrowSize (Vector3.one);
		// Store the player's final size
		float final_size = player.transform.localScale.magnitude;
		// The final size must be larger than the initial size
		Assert.True (final_size > initial_size);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Testing whether the player shrinks when the amt is negative.
	[Test]
	public void SizeShrinksOnNegativeAmount() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial size
		float initial_size = player.transform.localScale.magnitude;
		// Increase the player's size by negative amount
		player.GetComponent<PlayerController> ().GrowSize (-Vector3.one);
		// Store the player's final size
		float final_size = player.transform.localScale.magnitude;
		// The final size must be less than initial size
		Assert.True (final_size < initial_size);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Ensure when amt is zero, player's size doesn't change.
	[Test]
	public void SizeStaticOnZeroAmount() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial size
		float initial_size = player.transform.localScale.magnitude;
		// Increase the player's size by zero amount
		player.GetComponent<PlayerController> ().GrowSize (Vector3.zero);
		// Store the player's final size
		float final_size = player.transform.localScale.magnitude;
		// The final size must be equal to initial size
		Assert.True (final_size.AlmostEquals(initial_size, 0.005f));
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	/* 2. Ensuring the player's speed changes as the player's size changes.
	 * Smaller players will be faster. Larger players will be slower.
	 * The following unit tests will test whether the speed calculation function
	 * performs as required.
	 */
	// Ensure the speed of the player decreases when the size of the player increases
	[Test]
	public void SpeedDecreasesOnSizeIncrease() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial speed
		float initial_speed = player.GetComponent<PlayerController>().CurrentSpeed();
		// Increase the player's size by positive amount
		player.GetComponent<PlayerController> ().GrowSize (Vector3.one);
		// Store the player's final speed
		float final_speed = player.GetComponent<PlayerController> ().CurrentSpeed ();
		// The final speed must be smaller than the initial speed
		Assert.True (final_speed < initial_speed);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Ensure the speed of the player increases when the size of the player decreases 
	[Test]
	public void SpeedIncreasesOnSizeDecrease() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial speed
		float initial_speed = player.GetComponent<PlayerController>().CurrentSpeed();
		// Increase the player's size by negative amount
		player.GetComponent<PlayerController> ().GrowSize (-Vector3.one);
		// Store the player's final speed
		float final_speed = player.GetComponent<PlayerController> ().CurrentSpeed ();
		// The final speed must be larger than the initial speed
		Assert.True (final_speed > initial_speed);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Ensure the speed of the player stays the same when the size of the player remains the same
	[Test]
	public void SpeedStaticOnSizeStatic() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Store player's initial speed
		float initial_speed = player.GetComponent<PlayerController>().CurrentSpeed();
		// Increase the player's size by zero amount
		player.GetComponent<PlayerController> ().GrowSize (Vector3.zero);
		// Store the player's final speed
		float final_speed = player.GetComponent<PlayerController> ().CurrentSpeed ();
		// The final speed must be equal to the initial speed
		Assert.True (final_speed.AlmostEquals(initial_speed, 0.005f));
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	/* 3. Ensuring the camera reacts appropriately to the size changes of the player.
	 * Specifically, if the player gets bigger, the camera's height should increase.
	 * If the player gets smaller, the camera's height should decrease.
	 * Otherwise, the camera's height should remain static.
	 */

	// Checks whether camera zooms out on size increase.
	[Test]
	public void CameraRaisesOnSizeIncrease() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Initialize the target position
		player.GetComponent<PlayerController> ().targetPosition = Vector3.zero;
		// Store camera's current y position
		float initial_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// Increase the player's size by positive amount
		player.GetComponent<PlayerController> ().GrowSize (Vector3.one);
		// Store the camera's final y position
		float final_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// The final y position must be larger than the initial y position
		Assert.True (final_y > initial_y);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Checks whether camera zooms in on size decrease
	[Test]
	public void CameraLowersOnSizeDecrease() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Initialize the target position
		player.GetComponent<PlayerController> ().targetPosition = Vector3.zero;
		// Store camera's current y position
		float initial_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// Increase the player's size by negative amount
		player.GetComponent<PlayerController> ().GrowSize (-Vector3.one*20);
		// Store the camera's final y position
		float final_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// The final y position must be less than the initial y position
		Assert.True (final_y < initial_y);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Checks whether the camera remains constant when size remains constant
	[Test]
	public void CameraStaticOnSizeStatic() {
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Initialize the target position
		player.GetComponent<PlayerController> ().targetPosition = Vector3.zero;
		// Store camera's current y position
		float initial_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// Increase the player's size by nothing
		player.GetComponent<PlayerController> ().GrowSize (Vector3.zero);
		// Store the camera's final y position
		float final_y = player.GetComponent<PlayerController> ().targetPosition.y;
		// The final y position must be the same as the initial y position
		Assert.True (final_y.AlmostEquals(initial_y, 0.005f));
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	/* 4. The unit tests which ensure the player score increases properly. The score should increase
	 * only if it is larger than your current highest mass (since we record highest mass).
	 * It should never change if your size decreases or remains constant.
	 * 
	 */
	// Checks whether the the score increases properly
	[Test]
	public void ScoreIncreasesProperly() {
		// Assume your current hi score is 0 (i.e this is your first time playing the game
		float hi_score = 0.0f;
		float initial_score;
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Get your initial score 
		initial_score = player.GetComponent<PlayerController> ().CurrentScore (hi_score);
		// Increase the player's size by positive amt
		player.GetComponent<PlayerController> ().GrowSize (Vector3.one);
		// Get your final score
		float final_score = player.GetComponent<PlayerController> ().CurrentScore (initial_score);
		// The final score must be larger than initial score
		Assert.True (final_score > initial_score);
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Checks that the score remains static when size decreases 
	[Test]
	public void ScoreStaticWhenSizeDecreases() {
		// Assume your current hi score is 0 (i.e this is your first time playing the game
		float hi_score = 0.0f;
		float initial_score;
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Get your initial score 
		initial_score = player.GetComponent<PlayerController> ().CurrentScore (hi_score);
		// Decrease the player's size by positive amt
		player.GetComponent<PlayerController> ().GrowSize (-Vector3.one);
		// Get your final score
		float final_score = player.GetComponent<PlayerController> ().CurrentScore (initial_score);
		// The final score should be same as initial score
		Assert.True (final_score.AlmostEquals(initial_score, 0.005f));
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}

	// Checks that the score remains static when the size also remains static
	[Test]
	public void ScoreStaticWhenSizeStatic() {
		// Assume your current hi score is 0 (i.e this is your first time playing the game
		float hi_score = 0.0f;
		float initial_score;
		// Load the player 
		UnityEngine.Object player_o = Resources.Load ("PlayerCell");
		UnityEngine.Object camera_o = Resources.Load ("PlayerFollowCamera");
		GameObject player = UnityEngine.Object.Instantiate (player_o) as GameObject;
		GameObject camera = UnityEngine.Object.Instantiate (camera_o) as GameObject;
		// Initialize the player
		camera.GetComponent<CameraController> ().player = player;
		player.GetComponent<PlayerController> ().camera = camera;
		// Get your initial score 
		initial_score = player.GetComponent<PlayerController> ().CurrentScore (hi_score);
		// Decrease the player's size by nothing
		player.GetComponent<PlayerController> ().GrowSize (Vector3.zero);
		// Get your final score
		float final_score = player.GetComponent<PlayerController> ().CurrentScore (initial_score);
		// The final score should be same as initial score
		Assert.True (final_score.AlmostEquals(initial_score, 0.005f));
		// Destroy the objects 
		UnityEngine.Object.DestroyImmediate (player);
		UnityEngine.Object.DestroyImmediate (camera);
	}
}
