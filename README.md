## Project details ##
* Subject: IT Project COMP30022
* Application Name: Ballr
* Team Name: TeamCool

## Team details ##
* Rohan Sharma, 639271
* David Olsen, 641219
* Yash Narwal, 612840
* Jeremy Smith, 643667

## App details ##
* Platform: iOS, Android
* Game Engine: Unity3D
* Additional frameworks used: Photon Unity Networking (PUN), Unity Test tools (UTT)

## Our branches & code reviews ##

* We initially had 5 branches for this project
* Four of them got closed as we merged them into master
* Our code reviews can be found here: https://bitbucket.org/Comp30022ITCoolTeam/project/pull-requests/?displaystatus=merged

## Game description ##
In our game, initially you will be a small blob (sphere) free to roam a square map. You will be able to gain your size in several ways. One of the most easiest and inefficient ways is to eat the static food on the map (which changes color every few seconds). Another way is via PVP (player vs. player) interactions. If you collide with a player who is smaller than you (defined by some offset), you will gain a percentage of their mass and get bigger. However, if you collide with a play who is larger than you (once again defined by the same offset), you will die! There is another way you can get bigger; by shooting bullets. You are allowed to shoot bullets once every N (predefined) seconds. If your bullets hit another player, you gain a certain mass and they lose the same amount of mass. This allows you to fight players off from a distance and gain enough size before approaching and consuming them.


## File structure (Important folders and contents)##
* App - Contains our Unity web app
* Assets - Contains all our assets
* Assets/_Scenes - Contains all the scenes in our game
* Assets/Fonts - Contains all the fonts used in our game
* Assets/Materials - Contains the rendering materials for gameobjects within our game
* Assets/Menu - Contains most of the UI items
* Assets/PhotonUnityNetworking - The framework we used to implement networking.=
* Assets/Prefabs - Contains all prefabs within our game
* Assets/Scripts - Contains all the scripts within our game
* Assets/Textures - Contains all the textures within our game
* Assets/UnitTests - Contains all the unit tests for our game

## How do I get set up? ##
1. git clone https://bitbucket.org/Comp30022ITCoolTeam/  
2. cd project  
3. Open -> Roll A Ball in Unity 
4. Open Scene -> MenuScene 
5. Press Play

## How do I run the unit tests in this application? ##
0. Our unit tests are placed in Assets/UnitTests/Editor
1. Once you set up the application as shown above, go to step 2.
2. From the title bar, press Unity Test Tools. 
3. Then press Unit Test Runner.
4. Then press Run all.
5. This will execute all of our unit tests.